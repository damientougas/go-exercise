// DON'T CHANGE ANY CODE IN THIS FILE!

package sdk

import (
	"errors"

	"github.com/google/uuid"
)

type altairCloudSdk struct {
	machines       map[string]*Machine
	nameCounter    int
	stoppedCounter int
	errorCounter   int
}

// NewAltairCloudSDK creates a new instance of the Altair Cloud SDK
func NewAltairCloudSDK() CloudSDK {
	return &altairCloudSdk{
		machines: make(map[string]*Machine),
	}
}

func (e *altairCloudSdk) CreateMachine(name string) (string, error) {
	for _, v := range e.machines {
		if v.Name == name {
			return "", errors.New(ErrNotUnique)
		}
	}

	if e.errorCounter == 10 {
		e.errorCounter = 0
		return "", errors.New(ErrRuntimeError)
	} else {
		e.errorCounter++
	}

	m := &Machine{
		Id:    uuid.NewString(),
		Name:  name,
		State: StateRunning,
	}
	if e.stoppedCounter == 7 {
		m.State = StateStopped
		e.stoppedCounter = 0
	} else {
		e.stoppedCounter++
	}

	e.machines[m.Id] = m

	return m.Id, nil
}

func (e *altairCloudSdk) GetMachines() []*Machine {
	m := make([]*Machine, 0)
	for _, v := range e.machines {
		m = append(m, v)
	}
	return m
}

func (e *altairCloudSdk) DeleteMachine(id string) error {
	_, ok := e.machines[id]
	if !ok {
		return errors.New(ErrMachineNotFound)
	}
	delete(e.machines, id)
	return nil
}
