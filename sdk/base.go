// Package sdk is a cloud software development kit
//
// DON'T CHANGE ANY CODE IN THIS FILE!

package sdk

// CloudSDK is an interface that cloud providers implement in this package.
type CloudSDK interface {
	// CreateMachine creates a new machine in the cloud with the provided name. The ID of the created
	// machine is returned.
	CreateMachine(name string) (id string, err error)

	// GetMachines returns a list of all machines in the cloud
	GetMachines() (machines []*Machine)

	// DeleteMachine deletes a single cloud machine by ID
	DeleteMachine(id string) (err error)
}
