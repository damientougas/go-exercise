// DON'T CHANGE ANY CODE IN THIS FILE!

package sdk

type State int

const (
	StateStopped State = iota
	StateRunning

	ErrNotUnique       = "machine name is not unique"
	ErrMachineNotFound = "machine not found"
	ErrRuntimeError    = "runtime error"
)

// Machine is a data structure representing a machine in the cloud
type Machine struct {
	Id    string
	Name  string
	State State
}
