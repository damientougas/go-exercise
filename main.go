// Package main
//
// DON'T CHANGE ANY CODE IN THIS FILE!

package main

import (
	"exercise/sdk"
	"exercise/wrapper"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var test = map[string]int{
	"linux":   30,
	"windows": 100,
	"mac":     10,
}

func main() {
	w := wrapper.NewCustomWrapper()

	total := 0
	for k, v := range test {
		err := w.CreateMachines(k, v)
		if err != nil {
			exit("Create machines failed: " + err.Error())
		}
		total += v
	}

	names, err := w.ListMachineNames()
	if err != nil {
		exit("List machines failed: " + err.Error())
	}
	if len(names) != total {
		exit("Expected " + strconv.Itoa(total) + " machines, only found " + strconv.Itoa(len(names)))
	}

	counter := map[string]int{}
	for k, _ := range test {
		counter[k] = 0
	}

	for _, name := range names {
		m, err := w.GetMachine(name)
		if err != nil {
			exit("Unable to get machine by name: " + name)
		}
		found := false
		for k, _ := range test {
			if strings.HasPrefix(name, k) {
				counter[k] += 1
				found = true
			}
		}
		if !found {
			exit("Unrecognized machine prefix: " + name)
		}
		if m.State != sdk.StateRunning {
			exit("Machine is not running: " + name)
		}
	}

	for k, v := range test {
		if counter[k] != v {
			exit("Expected " + strconv.Itoa(v) + " " + k + " machines, only found " + strconv.Itoa(counter[k]))
		}
	}

	fmt.Println("SUCCESS!")
}

func exit(message string) {
	fmt.Println(message)
	os.Exit(1)
}
