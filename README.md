Instructions
============

Write a custom implementation of the `wrapper.CloudSDKWrapper` interface in the `wrapper` package. In `wrapper/custom.go` is a `wrapper.NewCustomWrapper` function that must return an instantiated version of your implementation. Do not change the name of this function, as `main.go` depends on it.

A few other things to note:

- You can test your code by compiling and running `main.go`. If you see the `SUCCESS!` message, then all tests have passed. If you see any other errors, then something is wrong and should be fixed.
- If you have a doubt about what behavior is expected, you can write what you understood in a comment.

Bonus Points
------------

- How well commented is your code?
- If you have time, can you write some unit tests to test your implementation?