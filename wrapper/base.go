// Package wrapper provides a custom wrapper around cloud APIs for creating machines in the cloud
//
// DON'T CHANGE ANY CODE IN THIS FILE!

package wrapper

import "exercise/sdk"

// CloudSDKWrapper defines a wrapper to be used over cloud SDKs. It is used to manage machines in the cloud,
// and should gracefully handle errors thrown my the cloud SDK that it wraps.
type CloudSDKWrapper interface {
	// GetMachine gets a single machine by name
	GetMachine(name string) (machine sdk.Machine, err error)

	// CreateMachines creates machines in the cloud, and will prefix all of their names with the prefix parameter. All
	// machines created with this function should be running. If the cloud provider returns a stopped machine, another
	// one must be created.
	CreateMachines(prefix string, count int) (err error)

	// ListMachineNames Returns the names of all machines in the cloud
	ListMachineNames() (names []string, err error)
}
