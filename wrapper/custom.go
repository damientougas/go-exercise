// This is where your exercise starts!

package wrapper

// NewCustomWrapper should return a wrapper.CloudSDKWrapper implementation that wraps the Altair Cloud SDK from
// the sdk package included in this project. Do not change the name of this function!
func NewCustomWrapper() CloudSDKWrapper {
	panic("Your code here!!!")
}
